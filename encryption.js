var crypto = require('crypto-js');

var secretMessage = "Hey how are you";
var secretObj = {
	"name":"ajay",
	"age" : 23
}
var secretKey = "yoBro123";

var encryptedMessage = crypto.AES.encrypt(secretMessage,secretKey);
console.log("EncryptedMessage :"+encryptedMessage);

var bytes = crypto.AES.decrypt(encryptedMessage,secretKey);
var decryptedMessage = bytes.toString(crypto.enc.Utf8);
console.log("DecryptedMessage :"+decryptedMessage);

var encryptedObj = crypto.AES.encrypt(JSON.stringify(secretObj),secretKey);
console.log("EncryptedObj :"+encryptedObj);


var bytesObj = crypto.AES.decrypt(encryptedObj,secretKey);
var decryptedObj = JSON.parse(bytesObj.toString(crypto.enc.Utf8));
console.log("DecryptedObj:"+decryptedObj.name);