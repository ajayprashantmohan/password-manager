console.log('starting password manager');
/*
commands for the following program

1. creating an account - "node app.js createAccount -n ajay -u ajay.mohan@intellectdesign.com -p ajaypassword -m masterpassword"
2. Getting an account - "node app.js getAccount -n ajay -m masterpassword"

*/
var crypto = require('crypto-js');
var storage = require('node-persist');
storage.initSync();

var argv = require('yargs')
.command('createAccount','creates an accounts',function(yargs){
	yargs.options({
		name:{
			demand:true,
			alias :'n',
			description:'name of the user',
			type:'string'
		},
		username:{
			demand:true,
			alias :'u',
			description:'username of the user',
			type:'string'
		},
		password:{
			demand:true,
			alias :'p',
			description:'password of the user',
			type:'string'
		},
		masterpassword:{
			demand:true,
			alias :'m',
			description:'master password of the user',
			type:'string'
		}

	}).help('help')
})
.command('getAccount','gets the account of the user',function(yargs){
	yargs.options({
		name:{
			demand:true,
			alias :'n',
			description:'name of the user',
			type:'string'
		},
		masterpassword:{
			demand:true,
			alias :'m',
			description:'master password of the user',
			type:'string'
		}
	}).help('help')
})
.help('help')
.argv;

var command = argv._[0];

function getAccounts(masterpassword){
	var encryptedAccount = storage.getItemSync('accounts');
	var accounts = [];
	if(typeof encryptedAccount != 'undefined'){
		var bytes = crypto.AES.decrypt(encryptedAccount,masterpassword);
		accounts = JSON.parse(bytes.toString(crypto.enc.Utf8));
	}
	return accounts;
}

function saveAccounts(accounts,masterpassword){
	var encryptedAccount = crypto.AES.encrypt(JSON.stringify(accounts),masterpassword);
	storage.setItemSync('accounts',encryptedAccount.toString());
	return accounts;
}

function createAccount(account,masterpassword){
	var accounts = getAccounts(masterpassword);
	accounts.push(account);
	saveAccounts(accounts,masterpassword)
	return account;
}

function getAccount(accountName,masterpassword){
	var accounts = getAccounts(masterpassword);
	var matchedAccount;

	accounts.forEach(function(account){
		if(account.name == accountName){
			matchedAccount = account;
		}
	});
	return matchedAccount;
}

if(command == 'createAccount'){
	try{
		createAccount({
			'name':argv.name,
			'username':argv.username,
			'password':argv.password
		},argv.masterpassword);
		console.log('Account Created');
	}
	catch(e){
		console.log("unable to create Account")
	}
	
}
else if (command == 'getAccount') {
	try{
			var fetchedAccount = getAccount(argv.name,argv.masterpassword);
		if(typeof fetchedAccount == 'undefined'){
			console.log('Account does not exist');
		}
		else{
			console.log(fetchedAccount);
		}
	}
	catch(e){
		console.log("unable to get Account")
	}
}
